# Weekly_Report


| Details of me |
| ------ | ------ |
| Name | Aienuddin |
| Matric Number | 198705 |
| Gender | Male |
| Subsystem | Simulation |
| Group | 2 |


# Week 1
- Agenda and Goals

| Agenda | Goals |
| ------ | ------ |
| cell | cell |
| cell | cell |

- Problem and Solution

| Problem | Solution |
| ------ | ------ |
| cell | cell |
| cell | cell |

- Work Progress

| Subsystem | Group |
| ------ | ------ |
| Share idea | Brainstorm for project |
| cell | cell |



# Week 2
- Agenda and Goals

| Agenda | Goals |
| ------ | ------ |
| Learn Gitlab | Easier to manage and configure file for project |
| Create Gantt Chart | Allows for better tracking |

- Problem and Solution

| Problem | Solution |
| ------ | ------ |
| Lack of skill in using Catia | Refer to manual |
| Lack of skill in using Solid Work | Refer to manual |
| The flow of project quite confussing | Discuss with team member and lecturer |
| Did not understand the tasks for each subsytem and group | Discuss with team member and lecturer |

- Work Progress

| Subsystem | Group |
| ------ | ------ |
| Learn Catia | Discuss about the flow of project |
| Learn Solid Work | Create Gitlab introduction and merging process |
| Create a Gantt Chart | Update the progress for each subsystem |


# Week 3
- **Agenda and Goals**

| Subsystem | Group|
| ------ | ------ |
| For this week, me, Haziq, and Wawa went to the lab to take a measurement of the airship. We fill up the airship with air and measure the length, width, height, and perimeter of each  section. Other than that, we also discussed with Fiqri the flow of work for the simulation subsystem. | We had assigned and divided our management roles, which were Asfeena as checker, Ivan Chua as process monitor, Tareena as recorder, and Nurul Hanis as coordinator. As usual, we had a weekly meeting  during class on (5/11/2021) with Dr Salah and Fiqri.|


- **Problem and Solution**
 
(Subsystem)

| Problem | Solution |
| ------ | ------ |
| Our problem is we cannot get the accurate sizing and measurement of airship because we did not measure the volume of air that fill into airship. We do not have tool to measure the capacity of air insert into airship, so the sizing of airship is unconstant every testing. Other than that, we can't get the accurate value because of our parallax error and we just  use the measure tape. | Our solution is by taking the perimeter measurement with a range of 10 cm.  We also refer the perimeter value of this airship from the template. |

(Group)

| Problem | Solution |
| ------ | ------ |
| The task is we need to divided the team role for each person in group| It wasn't difficult task because we just volunteer ourself to take a role for group for 2 weeks. It become more easier  since we recommended to use a duty roster which is rotate our role for every week. Here I attached  the excel link for my group management roles (https://docs.google.com/spreadsheets/d/11jA2B6f_ckZ-tsQE98Xk9M-lD-hem7uPyygdEp6gmRc/edit#gid=711576105) |


- **Work Progress**

| Subsystem Simulation | 
| ------ | 
| Designing HAU on CAD software| 
| Discuss the timeline of the progress in simulations (Real Time Measurements observing, data intake, Catia Programed Drawing or Integrated Designing, Performance Calculations of the HAU, Computational Fluid Dynamics Analysis (CFD), Ansys) | 


# Week 4

- **Agenda and Goals**

| Subsystem | Group|
| ------ | ------ |
| Consult Fiqri about the parameters required for simulation. | We had assigned and divided our management roles, which were Ivan as checker, Aienuddin as process monitor, Asfeena as recorder, and Nurul Hanis as coordinator. As usual, we had a weekly meeting  during class on (12/11/2021) with Dr Salah and Fiqri. |
| Run simulation on the cad model for 3D model| - |


- **Problem and Solution**

(Subsystem)

| Problem | Solution |
| ------ | ------ |
| Do not know how to use the Ansys Fluent since it is new to us | Learn from member that taking CFD subject and learn from internet.|
| We cannot get the best result because of meshing problem | We need to consider the skewness and orthogonal qualtiy during meshing process to get the best and logic result.|


- **Work Progress**

| Subsystem Simulation | 
| ------ | 
| We already run the simulation on ANSYS but, we need to find the possibilities parameters involved like lift, drag and thrust. We also refer books to get the paramater. | 



# Week 5

- **Agenda and Goals**

| Subsystem | Group|
| ------ | ------ |
| Run simulation on the cad model for 3D model. | We had assigned and divided our management roles, which were Aienuddin as checker, Aliff as process monitor, Ivan as recorder, and Nurul Hanis as coordinator. As usual, we had a weekly meeting  during class on (19/11/2021) with Dr Salah and Fiqri |
| Helping member to use ANSYS | Do a discussion for our work progression |

- **Problem and Solution**

(Subsystem)

| Problem | Solution |
| ------ | ------ |
| We get unexpected result from run simulation  | Ask senior and frriend to get the best result |
| The developing of meshing might affect the result | Need to consider the skewness and orthogonal qualtiy during meshing process |


- **Work Progress**

| Subsystem Simulation | 
| ------ | 
| We already did a run simulation on 3D model of airship on ANSYS |
| We already discuss with fiqri about the parameter that we want to consider for our initial setup |

# Week 6

- **Agenda and Goals**

| Subsystem | Group |
| ------ | ------ |
| Run simulation on the cad model for 3D HAU model | We had assigned and divided our management roles, which were Zaim as checker, Aliff as process monitor, Aienuddin as recorder, and Nurul Hanis as coordinator. As usual, we had a weekly meeting  during class on (26/11/2021) with Dr Salah and Fiqri |
| Run simulation with same initial setup but different meshing because we want to get the nicest and best trand of graph | Do a discussion for our work progression |

- **Problem and Solution**

(Subsystem)

| Problem | Solution |
| ------ | ------ |
| We did not know that out initial setup on ANSYS for run simulation of 3D model is correct or not | We need to improve our data for initial setup and imrpovise our skill of using ANSYS |
| The data and trend of the graph is wrong  | We need to improve our meshing and initial setup |


- **Work Progress**

| Subsystem Simulation | 
| ------ | 
| We already did a run simulation on 3D model of airship on ANSYS| 
| We plotting Cl vs angle of attack of 3D model | 


# Week 7

- **Agenda and Goals**

| Subsystem | Group |
| ------ | ------ |
| Run simulation on the cad model for 2D model | We had assigned and divided our management roles, which were Zaim as checker, Aliff as process monitor, Aienuddin as recorder, and Nurul Hanis as coordinator. As usual, we had a weekly meeting  during class on (26/11/2021) with Dr Salah and Fiqri |
| Run simulation with same initial setup but different meshing | Do a discussion for our work progression |

- **Problem and Solution**

(Subsystem)

| Problem | Solution |
| ------ | ------ |
| We did not know that out initial setup on ANSYS for run simulation of 3D model is correct or not | We did run simulation on 2D model on ANSYS because we want to compare the trend of graph from reference book. |
| The pattern of graph is quiet fluctuate  | We need to improve our meshing and initial setup |


- **Work Progress**

| Subsystem Simulation | 
| ------ | 
| We already did a run simulation on 2D model of airship on ANSYS| 
| We plotting Cl vs angle of attack to compare the graph with the referance book | 


# Week 11

- **Agenda and Goals**

| Subsystem | Group |
| ------ | ------ |
| We calculate the aerodynamics vector of airship based on equation from book | We had assigned and divided our management roles, which were Aienuddin as checker, Aliff as process monitor, Ivan as recorder, and Nurul Hanis as coordinator. As usual, we had a weekly meeting  during class on (9/11/2021) with Dr Salah and Fiqri |
| Aerodynamics vector calculated based on Jorgensen's equation | Do a discussion for our work progression |

- **Problem and Solution**

(Subsystem)

| Problem | Solution |
| ------ | ------ |
| we cannot figure out the relation between equation and value that we have. | We dicsuss and try to understand the equation and try to find as much as we can the data that needed. |
| The value of Aerodynamic vector quiet big  | We recheck the equation and value that we plug in |


- **Work Progress**

| Subsystem Simulation | 
| ------ | 
| We already calculate the value of aerodynamic vector of airship| 
| We plotting Cm pitching moment coefficient graph | 


